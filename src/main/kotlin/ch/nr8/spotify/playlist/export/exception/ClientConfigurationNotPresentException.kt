package ch.nr8.spotify.playlist.export.exception

class ClientConfigurationNotPresentException(message: String?) : Exception(message)
