package ch.nr8.spotify.playlist.export.controller

import ch.nr8.spotify.playlist.export.data.ClientData
import ch.nr8.spotify.playlist.export.service.AuthenticationService
import java.net.URI
import java.util.UUID
import org.jetbrains.annotations.NotNull
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping

@Controller
class AuthenticationController(private val authenticationService: AuthenticationService) {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Value(value = "\${spotify.api.client-id}")
    lateinit var spotifyApiClientId: String

    @Value(value = "\${spotify.api.default-response-type}")
    lateinit var spotifyApiDefaultResponseType: String

    @PostMapping(value = ["/configure/{userName}"])
    fun configure(
        @NotNull @PathVariable userName: String?
    ): ResponseEntity<String> {
        authenticationService.addClientConfiguration(
            ClientData(
                UUID.randomUUID(),
                spotifyApiClientId,
                userName,
                null,
                null,
                spotifyApiDefaultResponseType
            )
        )
        val message =
            "Successfully added configuration for user: $userName"
        logger.info(message)
        return ResponseEntity.ok(message)
    }

    @PostMapping(
        value = ["/authorize/{userName}/{scopes}"]
    )
    fun authorize(
        @NotNull @PathVariable userName: String,
        @NotNull @PathVariable scopes: String
    ): ResponseEntity<*>? {

        val authorizationCodeUriSync: URI =
            authenticationService.getAuthorizationCodeUri(userName, scopes)!!
        val message =
            "Please point your browser to this uri: $authorizationCodeUriSync"
        logger.info(message)
        return ResponseEntity.ok(message)
    }
}
