package ch.nr8.spotify.playlist.export.service

import com.wrapper.spotify.model_objects.specification.PlaylistSimplified
import com.wrapper.spotify.model_objects.specification.PlaylistTrack
import com.wrapper.spotify.model_objects.specification.Track
import java.io.BufferedWriter
import java.io.FileWriter
import java.time.LocalDateTime
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class PlaylistExportService(private val authenticationService: AuthenticationService) {

    private val logger = LoggerFactory.getLogger(javaClass)

    @Value(value = "\${spotify.export.file.prefix}")
    lateinit var filePrefix: String

    @Value(value = "\${spotify.export.file.suffix}")
    lateinit var fileSuffix: String

    @Value(value = "\${spotify.export.csv.delimiter}")
    lateinit var delimiter: String

    @Value(value = "\${spotify.export.items-per-request}")
    var itemsPerRequest: Int? = null

    @Value(value = "\${spotify.export.break-between-requests-ms}")
    var breakBetween: Long? = null

    fun export(userName: String) {
        logger.info("filename will be {}", buildFileName())

        val sb = StringBuilder()
        addHeader(sb)
        var index = 1
        var offset = 0
        val limit: Int = itemsPerRequest!!
        var run = true

        while (run) {

            val playlistSimplifiedPaging =
                authenticationService.getSpotifyApi(userName)
                    .listOfCurrentUsersPlaylists
                    .offset(offset)
                    .limit(limit)
                    .build()
                    .execute()

            if (playlistSimplifiedPaging.items.isEmpty()) {
                run = false
            }

            playlistSimplifiedPaging.items.forEach { playlistSimplified ->

                logger.info(
                    "got playlist ${playlistSimplified.name} from user ${playlistSimplified.owner.id}"
                )

                var trackOffset = 0
                val trackLimit: Int = itemsPerRequest!!
                var runTracks = true

                while (runTracks) {

                    val playlistTrackPaging =
                        authenticationService.getSpotifyApi(userName)
                            .getPlaylistsItems(
                                playlistSimplified.id
                            )
                            .offset(trackOffset)
                            .limit(trackLimit)
                            .build()
                            .execute()

                    if (playlistTrackPaging.items.isEmpty()) {
                        runTracks = false
                    }

                    playlistTrackPaging.items.forEach { playlistTrack ->
                        addTrack(sb, playlistSimplified, playlistTrack, index)
                    }

                    trackOffset += trackLimit
                    makePause()
                }
                index++
            }
            offset += limit
            makePause()
        }

        logger.info("got all playlists")
        BufferedWriter(FileWriter(buildFileName())).use { bw ->
            bw.write(sb.toString())
            bw.flush()
            logger.info("file written")
        }
    }

    private fun addTrack(
        sb: StringBuilder,
        playlist: PlaylistSimplified,
        playlistTrack: PlaylistTrack,
        index: Int
    ) {
        val track: Track = playlistTrack.track as Track
        sb.append(index)
        sb.append(delimiter)
        sb.append(playlist.name)
        sb.append(delimiter)
        sb.append(playlist.tracks.total)
        sb.append(delimiter)
        sb.append(track.name)
        sb.append(delimiter)
        track.artists.forEach { artist ->
            sb.append(artist.name)
            sb.append(", ")
        }
        sb.append(delimiter)
        sb.append(track.album.name)
        sb.append(delimiter)
        sb.append(track.durationMs)
        sb.append(delimiter)
        sb.append(playlist.owner.id)
        sb.append(delimiter)
        track.externalUrls.externalUrls.values.forEach {
            sb.append(it)
            sb.append(" ")
        }
        sb.append(System.lineSeparator())
    }

    private fun addHeader(sb: StringBuilder) {
        sb.append("Playlist Index")
        sb.append(delimiter)
        sb.append("Playlist Name")
        sb.append(delimiter)
        sb.append("Playlist Tracks Total")
        sb.append(delimiter)
        sb.append("Track Name")
        sb.append(delimiter)
        sb.append("Artists")
        sb.append(delimiter)
        sb.append("Track Album Name")
        sb.append(delimiter)
        sb.append("Track Duration (ms)")
        sb.append(delimiter)
        sb.append("Playlist Owner")
        sb.append(delimiter)
        sb.append("Track External Url")
        sb.append(System.lineSeparator())
    }

    private fun makePause() {
        try {
            Thread.sleep(breakBetween!!)
        } catch (e: InterruptedException) {
            logger.error(e.message, e)
            Thread.currentThread().interrupt()
        }
    }

    private fun buildFileName(): String {
        return filePrefix + LocalDateTime.now() + fileSuffix
    }
}
