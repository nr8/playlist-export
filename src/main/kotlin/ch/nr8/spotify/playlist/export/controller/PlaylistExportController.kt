package ch.nr8.spotify.playlist.export.controller

import ch.nr8.spotify.playlist.export.service.PlaylistExportService
import org.jetbrains.annotations.NotNull
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@Controller
class PlaylistExportController(private val playlistExportService: PlaylistExportService) {

    @GetMapping("/export/{userName}")
    fun export(
        @NotNull @PathVariable userName: String
    ): ResponseEntity<*>? {
        playlistExportService.export(userName)
        return ResponseEntity.ok().build<Any>()
    }
}
