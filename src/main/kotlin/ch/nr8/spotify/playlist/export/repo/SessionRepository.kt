package ch.nr8.spotify.playlist.export.repo

import ch.nr8.spotify.playlist.export.entity.Session
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository

interface SessionRepository : JpaRepository<Session, Long> {
    fun findByClientConfigurationUserName(configurationName: String): Optional<Session>
}
