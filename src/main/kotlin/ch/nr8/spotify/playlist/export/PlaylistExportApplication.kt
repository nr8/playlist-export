package ch.nr8.spotify.playlist.export

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PlaylistExportApplication
    fun main(args: Array<String>) {
        runApplication<PlaylistExportApplication>(*args)
    }
