package ch.nr8.spotify.playlist.export.entity

import java.time.LocalDateTime
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
data class Session(
    @Id
    val id: UUID,
    val clientName: String,
    val accessToken: String,
    val refreshToken: String,
    val tokenType: String,
    val expiryTime: LocalDateTime,
    @ManyToOne
    val clientConfiguration: ClientConfiguration
)
