package ch.nr8.spotify.playlist.export.data

import java.util.UUID

data class ClientData(
    val id: UUID?,
    val clientId: String,
    val userName: String?,
    val secret: String?,
    val redirect: String?,
    val responseType: String
)
