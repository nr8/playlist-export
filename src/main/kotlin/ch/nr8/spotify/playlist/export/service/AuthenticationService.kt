package ch.nr8.spotify.playlist.export.service

import ch.nr8.spotify.playlist.export.data.ClientData
import ch.nr8.spotify.playlist.export.entity.ClientConfiguration
import ch.nr8.spotify.playlist.export.entity.Session
import ch.nr8.spotify.playlist.export.exception.ClientConfigurationNotPresentException
import ch.nr8.spotify.playlist.export.repo.ClientConfigurationRepository
import ch.nr8.spotify.playlist.export.repo.SessionRepository
import com.wrapper.spotify.SpotifyApi
import com.wrapper.spotify.SpotifyHttpManager
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest
import java.net.URI
import java.time.LocalDateTime
import java.util.Base64
import java.util.Optional
import java.util.UUID
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Service
class AuthenticationService(
    private val sessionRepository: SessionRepository,
    private val configurationRepository: ClientConfigurationRepository
) {

    @Value(value = "\${spotify.api.client-id}")
    lateinit var spotifyApiClientId: String

    @Value(value = "\${spotify.api.client-secret}")
    lateinit var spotifyApiClientSecret: String

    @Value(value = "\${spotify.api.redirect-uri}")
    lateinit var spotifyApiRedirectUri: String

    fun getSpotifyApi(userName: String): SpotifyApi {
        val configurationOptional: Optional<ClientConfiguration> =
            configurationRepository.findByUserName(userName)

        if (configurationOptional.isPresent) {
            val configuration = configurationOptional.get()
            val spotifyApi: SpotifyApi = SpotifyApi.Builder()
                .setClientId(configuration.clientId)
                .setClientSecret(spotifyApiClientSecret)
                .setRedirectUri(
                    SpotifyHttpManager.makeUri(
                        spotifyApiRedirectUri
                    )
                )
                .build()
            setTokens(userName, spotifyApi)
            return spotifyApi
        }
        throw ClientConfigurationNotPresentException("Please add configuration for client $userName")
    }

    fun addClientConfiguration(clientData: ClientData) {
        configurationRepository.save(
            ClientConfiguration(clientData)
        )
    }

    fun getAuthorizationCodeUri(
        userName: String,
        scope: String
    ): URI? {

        val stateFromUserName =
            Base64.getEncoder().withoutPadding().encodeToString(userName.toByteArray())

        val authorizationCodeUriRequest =
            getSpotifyApi(userName).authorizationCodeUri()
                .client_id(spotifyApiClientId)
                .state(stateFromUserName)
                .scope(scope)
                .show_dialog(false)
                .build()

        return authorizationCodeUriRequest.execute()
    }

    fun authorizeApi(
        userName: String,
        authorizationCodeRequest: AuthorizationCodeRequest
    ) {
        val authorizationCodeCredentials = authorizationCodeRequest.execute()
        val expiresInSeconds = authorizationCodeCredentials.expiresIn.toLong()
        val expiresIn = LocalDateTime.now().plusSeconds(expiresInSeconds)
        val configurationOptional: Optional<ClientConfiguration> =
            configurationRepository.findByUserName(userName)
        if (configurationOptional.isPresent) {
            val session = Session(
                UUID.randomUUID(),
                userName,
                authorizationCodeCredentials.accessToken,
                authorizationCodeCredentials.refreshToken,
                authorizationCodeCredentials.tokenType,
                expiresIn,
                configurationOptional.get()
            )
            sessionRepository.save(session)
        } else {
            throw ClientConfigurationNotPresentException(
                "Configuration not present for client $userName"
            )
        }
    }

    private fun setTokens(
        clientName: String,
        spotifyApi: SpotifyApi
    ) {
        val sessionOptional: Optional<Session> =
            sessionRepository.findByClientConfigurationUserName(clientName)
        if (sessionOptional.isPresent &&
            sessionOptional.get().expiryTime.isAfter(LocalDateTime.now())
        ) {
            spotifyApi.accessToken = sessionOptional.get().accessToken
            spotifyApi.refreshToken = sessionOptional.get().refreshToken
        }
    }
}
