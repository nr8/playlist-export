package ch.nr8.spotify.playlist.export.repo

import ch.nr8.spotify.playlist.export.entity.ClientConfiguration
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository

interface ClientConfigurationRepository : JpaRepository<ClientConfiguration, Long> {
    fun findByUserName(name: String): Optional<ClientConfiguration>
}
