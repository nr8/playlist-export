package ch.nr8.spotify.playlist.export.controller

import ch.nr8.spotify.playlist.export.service.AuthenticationService
import java.util.Base64
import org.jetbrains.annotations.NotNull
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
class CallbackController(private val authenticationService: AuthenticationService) {

    @GetMapping("/callback")
    fun callback(
        @NotNull @RequestParam code: String,
        @NotNull @RequestParam state: String
    ): ResponseEntity<String>? {
        val userName = String(Base64.getDecoder().decode(state))
        val spotifyApi = authenticationService.getSpotifyApi(userName)
        val authorizationCodeRequest = spotifyApi.authorizationCode(code).build()
        authenticationService.authorizeApi(userName, authorizationCodeRequest)
        return ResponseEntity.ok("API successfully authorized")
    }
}
