package ch.nr8.spotify.playlist.export.entity

import ch.nr8.spotify.playlist.export.data.ClientData
import java.util.UUID
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class ClientConfiguration(
    @Id
    val id: UUID,
    val userName: String,
    val clientId: String,
    val clientSecret: String?,
    val redirectUri: String?,
    val responseType: String,
    val showDialog: Boolean
) {
    constructor(clientData: ClientData) : this(
        id = clientData.id!!,
        userName = clientData.userName!!,
        clientId = clientData.clientId,
        clientSecret = clientData.secret,
        redirectUri = clientData.redirect,
        responseType = "token",
        showDialog = false
    )
}
