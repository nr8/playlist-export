package ch.nr8.spotify.playlist.export.helper

import ch.nr8.spotify.playlist.export.data.ClientData
import java.util.UUID

object ClientDataFactory {

    val id: UUID = UUID.randomUUID()
    const val clientId = "clientId"
    const val clientName = "susi"
    const val secret = "secret"
    const val redirect = "redirect"
    const val responseType = "token"

    fun makeClientData(): ClientData {
        return ClientData(id, clientId, clientName, secret, redirect, responseType)
    }
}
