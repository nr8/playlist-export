package ch.nr8.spotify.playlist.export.repo

import ch.nr8.spotify.playlist.export.helper.ClientConfigurationFactory
import ch.nr8.spotify.playlist.export.helper.SessionFactory
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class SessionRepositoryTest {

    @Autowired
    lateinit var sessionRepository: SessionRepository

    @Autowired
    lateinit var clientConfigurationRepository: ClientConfigurationRepository

    @BeforeEach
    fun cleanUp() {
        sessionRepository.deleteAll()
        clientConfigurationRepository.deleteAll()
    }

    @Test
    fun findByClientConfigurationName() {

        val saveSession = SessionFactory.makeSession()
        clientConfigurationRepository.save(saveSession.clientConfiguration)
        sessionRepository.save(saveSession)

        val sessionOptional = sessionRepository
            .findByClientConfigurationUserName(ClientConfigurationFactory.name)

        assertTrue(sessionOptional.isPresent)

        val session = sessionOptional.get()
        assertEquals(saveSession.id, session.id)
        assertEquals(ClientConfigurationFactory.name, session.clientName)
        assertEquals(SessionFactory.accessToken, session.accessToken)
        assertEquals(SessionFactory.expiryTime, session.expiryTime)
        assertEquals(SessionFactory.clientName, session.clientConfiguration.userName)
        assertEquals(SessionFactory.refreshToken, session.refreshToken)
        assertEquals(SessionFactory.tokenType, session.tokenType)
    }

    @Test
    fun findByClientConfigurationNameNotFound() {
        val sessionOptional = sessionRepository
            .findByClientConfigurationUserName("anna")
        assertFalse(sessionOptional.isPresent)
    }
}
