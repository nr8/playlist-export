package ch.nr8.spotify.playlist.export.helper

import ch.nr8.spotify.playlist.export.entity.ClientConfiguration
import java.util.UUID

object ClientConfigurationFactory {

    val id: UUID = UUID.randomUUID()
    const val name = "susi"
    const val clientId = "clientId"
    const val clientSecret = "clientSecret"
    const val redirectUri = "redirectUrl"
    const val responseType = "responseType"
    const val showDialog = false

    fun makeClientConfiguration(): ClientConfiguration {
        return ClientConfiguration(
            id, name, clientId, clientSecret,
            redirectUri, responseType, showDialog
        )
    }
}
