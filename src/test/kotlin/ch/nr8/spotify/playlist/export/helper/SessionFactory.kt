package ch.nr8.spotify.playlist.export.helper

import ch.nr8.spotify.playlist.export.entity.Session
import java.time.LocalDateTime
import java.util.UUID

object SessionFactory {

    val id: UUID = UUID.randomUUID()
    const val clientName = "susi"
    const val accessToken = "token"
    const val refreshToken = "refresh"
    const val tokenType = "type"
    val expiryTime: LocalDateTime = LocalDateTime.now().plusHours(12L)

    fun makeSession(): Session {
        val clientConfiguration = ClientConfigurationFactory.makeClientConfiguration()
        return Session(
            id, clientName, accessToken, refreshToken, tokenType, expiryTime, clientConfiguration
        )
    }
}
