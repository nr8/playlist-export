package ch.nr8.spotify.playlist.export.controller

import ch.nr8.spotify.playlist.export.service.AuthenticationService
import com.wrapper.spotify.SpotifyApi
import com.wrapper.spotify.SpotifyHttpManager
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@WebMvcTest(CallbackController::class)
internal class CallbackControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var mockAuthenticationService: AuthenticationService

    private val username = "susi"

    @Test
    fun callback() {

        val spotifyApi: SpotifyApi = SpotifyApi.Builder()
            .setClientId("clientId")
            .setClientSecret("clientSecret")
            .setRedirectUri(
                SpotifyHttpManager.makeUri("redirectUri")
            )
            .build()

        given(mockAuthenticationService.getSpotifyApi(username)).willReturn(spotifyApi)

        mvc.perform(
            MockMvcRequestBuilders.get(
                "/callback?code=an-authorization-code-from-spotify&state=c3VzaQ"
            )
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
    }
}
