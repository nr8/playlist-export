package ch.nr8.spotify.playlist.export.data

import ch.nr8.spotify.playlist.export.helper.ClientDataFactory
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class ClientDataTest {

    @Test
    fun getId() {
        val clientData = ClientDataFactory.makeClientData()

        assertEquals(ClientDataFactory.id, clientData.id)
        assertEquals(ClientDataFactory.clientId, clientData.clientId)
        assertEquals(ClientDataFactory.clientName, clientData.userName)
        assertEquals(ClientDataFactory.secret, clientData.secret)
        assertEquals(ClientDataFactory.redirect, clientData.redirect)
        assertEquals(ClientDataFactory.responseType, clientData.responseType)
    }
}
