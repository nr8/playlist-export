package ch.nr8.spotify.playlist.export.repo

import ch.nr8.spotify.playlist.export.helper.ClientConfigurationFactory
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class ClientConfigurationRepositoryTest {

    @Autowired
    lateinit var clientConfigurationRepository: ClientConfigurationRepository

    @Autowired
    lateinit var sessionRepository: SessionRepository

    @BeforeEach
    fun cleanUp() {
        sessionRepository.deleteAll()
        clientConfigurationRepository.deleteAll()
    }

    @Test
    fun findByName() {

        val newClientConfiguration = ClientConfigurationFactory.makeClientConfiguration()
        clientConfigurationRepository.save(newClientConfiguration)

        val clientConfigurationOptional =
            clientConfigurationRepository.findByUserName(ClientConfigurationFactory.name)

        assertTrue(clientConfigurationOptional.isPresent)

        val clientConfiguration = clientConfigurationOptional.get()

        assertEquals(ClientConfigurationFactory.id, clientConfiguration.id)
        assertEquals(ClientConfigurationFactory.clientId, clientConfiguration.clientId)
        assertEquals(ClientConfigurationFactory.clientSecret, clientConfiguration.clientSecret)
        assertEquals(ClientConfigurationFactory.name, clientConfiguration.userName)
        assertEquals(ClientConfigurationFactory.redirectUri, clientConfiguration.redirectUri)
        assertEquals(ClientConfigurationFactory.responseType, clientConfiguration.responseType)
        assertEquals(ClientConfigurationFactory.showDialog, clientConfiguration.showDialog)
        assertEquals(ClientConfigurationFactory.id, clientConfiguration.id)
    }

    @Test
    fun findByNameNotPresent() {
        val optional = clientConfigurationRepository.findByUserName("susi")
        assertFalse(optional.isPresent)
    }
}
