package ch.nr8.spotify.playlist.export.service

import ch.nr8.spotify.playlist.export.exception.ClientConfigurationNotPresentException
import ch.nr8.spotify.playlist.export.helper.ClientConfigurationFactory
import ch.nr8.spotify.playlist.export.helper.ClientDataFactory
import ch.nr8.spotify.playlist.export.helper.SessionFactory
import ch.nr8.spotify.playlist.export.repo.ClientConfigurationRepository
import ch.nr8.spotify.playlist.export.repo.SessionRepository
import com.wrapper.spotify.model_objects.credentials.AuthorizationCodeCredentials
import com.wrapper.spotify.requests.authorization.authorization_code.AuthorizationCodeRequest
import java.time.LocalDateTime
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.Resource

@SpringBootTest
internal class AuthenticationServiceTest {

    @Autowired
    lateinit var authenticationService: AuthenticationService

    @Autowired
    lateinit var clientConfigurationRepository: ClientConfigurationRepository

    @Autowired
    lateinit var sessionRepository: SessionRepository

    @Value("classpath:credentials.json")
    lateinit var resourceFile: Resource

    private val userName = "susi"

    @BeforeEach
    fun cleanUp() {
        sessionRepository.deleteAll()
        clientConfigurationRepository.deleteAll()
    }

    @Test
    fun getSpotifyApiWithSession() {

        val clientConfiguration = ClientConfigurationFactory.makeClientConfiguration()
        clientConfigurationRepository.save(clientConfiguration)
        val session = SessionFactory.makeSession()
        sessionRepository.save(session)

        val spotifyApi = authenticationService.getSpotifyApi(userName)
        assertNotNull(spotifyApi)
        assertEquals(ClientConfigurationFactory.clientId, spotifyApi.clientId)
        assertEquals("7977987fed4944f0bd00058b8da89768", spotifyApi.clientSecret)
        assertEquals(SessionFactory.accessToken, spotifyApi.accessToken)
        assertEquals(SessionFactory.refreshToken, spotifyApi.refreshToken)
        assertEquals("https", spotifyApi.scheme)
        assertEquals("api.spotify.com", spotifyApi.host)
        assertEquals(443, spotifyApi.port)
    }

    @Test
    fun getSpotifyApiWithoutSession() {
        clientConfigurationRepository.save(ClientConfigurationFactory.makeClientConfiguration())
        val spotifyApi = authenticationService.getSpotifyApi(userName)

        assertNotNull(spotifyApi)
    }

    @Test
    fun getSpotifyApiException() {
        assertThrows(
            ClientConfigurationNotPresentException::class.java
        ) {
            authenticationService.getSpotifyApi(userName)
        }
    }

    @Test
    fun addClientConfiguration() {
        addClientConfigurationToRepo()

        val optional = clientConfigurationRepository.findByUserName(ClientDataFactory.clientName)
        assertTrue(optional.isPresent)

        val clientConfiguration = optional.get()
        assertEquals(ClientDataFactory.id, clientConfiguration.id)
        assertEquals(ClientDataFactory.clientName, clientConfiguration.userName)
        assertEquals(ClientDataFactory.secret, clientConfiguration.clientSecret)
        assertEquals(ClientDataFactory.redirect, clientConfiguration.redirectUri)
        assertEquals(ClientDataFactory.clientId, clientConfiguration.clientId)
        assertEquals(ClientDataFactory.responseType, clientConfiguration.responseType)
        assertFalse(clientConfiguration.showDialog)
    }

    @Test
    fun getAuthorizationCodeUri() {
        addClientConfigurationToRepo()
        val scopes = "playlist-read-private%2Cuser-read-private"
        val authorizationCodeUri =
            authenticationService.getAuthorizationCodeUri(ClientDataFactory.clientName, scopes)
        assertNotNull(authorizationCodeUri)
        assertEquals(
            "https://accounts.spotify.com:443/authorize?" +
                    "response_type=code&" +
                    "redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fcallback&" +
                    "client_id=c63ea5beee7d40b09347afd8f0c6de3f&" +
                    "state=c3VzaQ&" +
                    "scope=playlist-read-private%252Cuser-read-private" +
                    "&show_dialog=false",
            authorizationCodeUri.toString()
        )
    }

    @Test
    fun authorizeApi() {

        val clientData = ClientDataFactory.makeClientData()
        authenticationService.addClientConfiguration(clientData)

        val authorizationCodeCredentials =
            AuthorizationCodeCredentials.JsonUtil().createModelObject(resourceFile.file.readText())

        authenticationService.authorizeApi(
            userName,
            getAuthorizationCodeRequestMock(authorizationCodeCredentials)
        )

        val optional = sessionRepository.findByClientConfigurationUserName(clientData.userName!!)
        assertTrue(optional.isPresent)
        val session = optional.get()

        assertNotNull(session.id)
        assertEquals(clientData.userName, session.clientName)
        assertEquals(clientData.userName, session.clientConfiguration.userName)
        assertEquals(authorizationCodeCredentials.accessToken, session.accessToken)
        assertEquals(authorizationCodeCredentials.refreshToken, session.refreshToken)
        assertTrue(session.expiryTime.isAfter(LocalDateTime.now()))
    }

    @Test
    fun authorizeApiException() {

        val authorizationCodeCredentials =
            AuthorizationCodeCredentials.JsonUtil().createModelObject(resourceFile.file.readText())

        assertThrows(
            ClientConfigurationNotPresentException::class.java
        ) {
            authenticationService.authorizeApi(
                userName,
                getAuthorizationCodeRequestMock(authorizationCodeCredentials)
            )
        }
    }

    private fun getAuthorizationCodeRequestMock(
        authorizationCodeCredentials: AuthorizationCodeCredentials
    ): AuthorizationCodeRequest {

        val authorizationCodeRequest = mock(AuthorizationCodeRequest::class.java)
        `when`(
            authorizationCodeRequest.execute()
        ).thenReturn(authorizationCodeCredentials)

        return authorizationCodeRequest
    }

    private fun addClientConfigurationToRepo() {
        val clientData = ClientDataFactory.makeClientData()
        authenticationService.addClientConfiguration(clientData)
    }
}
