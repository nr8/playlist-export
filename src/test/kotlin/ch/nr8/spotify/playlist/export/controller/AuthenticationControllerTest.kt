package ch.nr8.spotify.playlist.export.controller

import ch.nr8.spotify.playlist.export.service.AuthenticationService
import java.net.URI
import org.junit.jupiter.api.Test
import org.mockito.BDDMockito.given
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(AuthenticationController::class)
internal class AuthenticationControllerTest {

    @Autowired
    lateinit var mvc: MockMvc

    @MockBean
    lateinit var authenticationService: AuthenticationService

    private val userName = "test.user"

    @Test
    fun configure() {
        mvc.perform(
            post("/configure/$userName")
        ).andExpect(status().isOk)
    }

    @Test
    fun authorize() {

        val scopes = "playlist-read-private,user-read-private"
        val uri = URI("http://this-is-the-uri")

        given(authenticationService.getAuthorizationCodeUri(userName, scopes)).willReturn(uri)

        mvc.perform(
            post("/authorize/$userName/$scopes")
        ).andExpect(status().isOk)
            .andExpect(
                content()
                    .string(
                        "Please point your browser to this uri: http://this-is-the-uri"
                    )
            )
    }
}
